import javafx.application.Application;
import javafx.scene.Scene;
import javafx.scene.layout.Pane;
import javafx.scene.paint.Color;
import javafx.scene.shape.Rectangle;
import javafx.scene.text.Text;
import javafx.stage.Stage;
import java.util.Scanner;


public class Rectangles extends Application {
    @Override
    public void start(Stage primaryStage) {
        Scanner info = new Scanner(System.in);
        Pane pane = new Pane();

        // Get user input for the Rectangles.
        System.out.println("Enter the center x value for the rectangle ");
        int x_center1 = info.nextInt();
        System.out.println("Enter the center y value for the rectangle ");
        int y_center1 = info.nextInt();
        System.out.println("Enter the width of the rectangle ");
        int width1 = info.nextInt();
        System.out.println("Enter the height of the rectangle ");
        int height1 = info.nextInt();
        System.out.println();

        // Rectangle two
        System.out.println("Enter the center x value for the rectangle ");
        int x_center2 = info.nextInt();
        System.out.println("Enter the center y value for the rectangle ");
        int y_center2 = info.nextInt();
        System.out.println("Enter the width of the rectangle ");
        int width2 = info.nextInt();
        System.out.println("Enter the height of the rectangle ");
        int height2 = info.nextInt();
        System.out.println();

        info.close();

        // Put the rectangles together
        Rectangle rect1 = new Rectangle(x_center1, y_center1, width1, height1);
        rect1.setFill(Color.TRANSPARENT);
        rect1.setStroke(Color.PURPLE);
        rect1.setStrokeWidth(3);
        Rectangle rect2 = new Rectangle(x_center2, y_center2, width2, height2);
        rect2.setFill(Color.TRANSPARENT);
        rect2.setStroke(Color.RED);
        rect2.setStrokeWidth(3);

        // This function does the calculations about their relative
        // locations. It is defined below.
        int spots = logic(rect1, rect2);

        // Then I change the text based on the return value of logic.
        Text status = new Text();
        status.setX(20);
        status.setY(20);
        if (spots == 1) {
            status.setText("These rectangles are not touching, or inside of each other.");
        }
        else if (spots == 0) {
            status.setText("One of these rectangles is inside of the other.");
        }
        else {
            status.setText("These rectangles are touching.");
        }

        // Make them a pane
        pane.getChildren().addAll(rect1, rect2, status);

        // Put them on the Scene(
        Scene scene = new Scene(pane, 1600, 800);
        // And then the stage
        primaryStage.setTitle("Rectangles GUI");
        primaryStage.setScene(scene);
        primaryStage.show();
    }

    public int logic(Rectangle rect1, Rectangle rect2) {
        double y1 = rect1.getY();
        double x1 = rect1.getX();
        double y2 = rect2.getY();
        double x2 = rect2.getX();
        double ey1 = rect1.getY() + rect1.getHeight();
        double ex1 = rect1.getX() + rect1.getWidth();
        double ey2 = rect2.getY() + rect2.getHeight();
        double ex2 = rect2.getX() + rect2.getWidth();

        if (y1 + rect1.getHeight() < y2 || x1 + rect1.getWidth() < x2) {
            return 1;
        }
        else if (y2 + rect2.getHeight() < y1 || x2 + rect2.getWidth() < x1) {
            return 1;
        }
        else if (x2 > x1 && y2 > y1 && ex2 < ex1 && ey2 < ey1) {
            return 0;
        }
        else if (x1 > x2 && y1 > y2 && ex1 < ex2 && ey1 < ey2) {
            return 0;
        }
        else {
            return -1;
        }

    }
    public static void main(String[] args) {
        launch(args);
    }
}