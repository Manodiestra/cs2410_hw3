import javafx.application.Application;
import javafx.scene.Scene;
import javafx.scene.layout.Pane;
import javafx.scene.paint.Color;
import javafx.scene.text.Font;
import javafx.scene.text.FontWeight;
import javafx.scene.text.Text;
import javafx.stage.Stage;
import java.awt.geom.Point2D;


public class WelcomeToJava extends Application {
    @Override
    public void start(Stage primaryStage) {

        // Create a list of characters so I can interacts with each one.
        String[] phrase = "WELCOME TO JAVA".split("");

        // And of course we will need a pane
        Pane myPane = new Pane();

        //Setting up data for the Point2D object to use
        Point2D spots = new Point2D.Double(300,300);
        double radious = 150;
        double angle = 0;
        double rotate = 90;
        double x, y;
        Text character;

        // Here I pull out each item from the array of characters and rotate them slightly
        // as well as give them new coordinates.
        for (int i = 0; i < phrase.length; i++, rotate += 22, angle += 22){
            x = spots.getX() + radious * Math.cos(3.14*angle/180);
            y = spots.getY() + radious * Math.sin(3.14*angle/180);
            character = new Text(x, y, phrase[i]);
            character.setFill(Color.web("#0F2439"));
            character.setFont(Font.font("Algerian", FontWeight.EXTRA_BOLD, 50));
            character.setRotate(rotate);
            myPane.getChildren().add(character);
        }

        // Then add it all to the Scene and Stage
        Scene scene = new Scene(myPane, 700, 600);
        primaryStage.setTitle("Welcome To Java GUI");
        primaryStage.setScene(scene);
        primaryStage.show();
    }

    public static void main(String[] args) {
        launch(args);
    }
}