import javafx.application.Application;
import javafx.scene.Scene;
import javafx.scene.layout.Pane;
import javafx.scene.paint.Color;
import javafx.scene.shape.Arc;
import javafx.scene.shape.ArcType;
import javafx.scene.shape.Line;
import javafx.stage.Stage;
import javafx.scene.shape.Ellipse;


public class Hangman extends Application {
    @Override
    public void start(Stage primaryStage) {
        Pane myPane = new Pane();

        // Make the guy
        Ellipse base = new Ellipse();
        Ellipse head = new Ellipse(700,240,50,50);
        Line body = new Line(700, 290, 700, 490);
        body.setStrokeWidth(8);
        Line right_leg = new Line(700, 490, 760, 590);
        right_leg.setStrokeWidth(8);
        Line left_leg = new Line(700, 490, 640, 590);
        left_leg.setStrokeWidth(8);
        Line right_arm = new Line(700, 320, 790, 300);
        right_arm.setStrokeWidth(8);
        Line left_arm = new Line(700, 320, 640, 400);
        left_arm.setStrokeWidth(8);

        // Make the Hang Stand
        Line hang_pole = new Line(400, 100, 400, 520);
        hang_pole.setStrokeWidth(10);
        hang_pole.setStroke(Color.web("0F2439"));
        Arc pole_base = new Arc(400, 550, 150, 30,0, 180);
        pole_base.setStrokeWidth(10);
        pole_base.setStroke(Color.web("#0F2439"));
        pole_base.setType(ArcType.OPEN);
        pole_base.setFill(Color.TRANSPARENT);
        Line pole_head = new Line(400, 100, 700, 100);
        pole_head.setStrokeWidth(10);
        pole_head.setStroke(Color.web("#0F2439"));
        Line rope = new Line(700, 108, 700, 186);
        rope.setStroke(Color.WHEAT);
        rope.setStrokeWidth(5);

        // Add it all to the Pand, scene, and stage
        myPane.getChildren().addAll(right_leg, body, head, left_leg, right_arm, left_arm,
                hang_pole, pole_base, pole_head, rope);
        Scene scene = new Scene(myPane, 1400, 800);
        primaryStage.setTitle("Hangman GUI");
        primaryStage.setScene(scene);
        primaryStage.show();
    }

    public static void main(String[] args) {
        launch(args);
    }
}